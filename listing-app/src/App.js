import './App.css';
import { listForSale } from "./list-nfts";

function App() {

  function list(start, stop) {
    for (let i=start; i<=stop; i++) {
      listForSale(i)
          .then(p => {
            console.log('listed')
          })
          .catch(ex => {
            console.log("Failed")
            console.log(ex)
          })
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <h1>List dem damn NFTs</h1>
        <p>Includes both the IDs of start and stop.</p>
        <input id="start" type="number" placeholder="Start Token Id"/>
        <input id="stop" type="number" placeholder="Stop Token Id"/>
        <button onClick={() => list(document.getElementById('start').value, document.getElementById('stop').value)}>List</button>
        {/*<button onClick={listForSale(document.getElementById('start'))}>List</button>*/}
      </header>
    </div>
  );
}

export default App;

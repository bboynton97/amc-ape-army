import * as Web3 from 'web3'
import { OpenSeaPort, Network } from 'opensea-js'

// This example provider won't let you make transactions, only read-only calls:
// const provider = new Web3.providers.HttpProvider('window.ethereum')
// const provider = new Web3.providers.HttpProvider('https://xwtylss9sfu3.usemoralis.com:2053/server')
const provider = new Web3.providers.HttpProvider('https://speedy-nodes-nyc.moralis.io/c12438b53698a63f1a7045c4/polygon/mumbai')

const seaport = new OpenSeaPort(provider, {
    networkName: Network.Main
})

const _bradsWallet = "0xf7b2453D2846955Fc2123701C57c08351719bFf6"
// const _tokenAddress = "0x48Ea9f1efb967c326B29b2BB2cF9E0755Db355C8" // contract address
// const _tokenAddress = "0x48ea9f1efb967c326b29b2bb2cf9e0755db355c8" // contract address
const _tokenAddress = "0x60e4d786628fea6478f785a6d7e704777c86a7c6" // bored ape contract address

export async function listForSale(tokenId) {
    const asset = await seaport.api.getAsset({
        tokenAddress:_tokenAddress, // string
        tokenId, // string | number | null
    })
    console.log(JSON.stringify(asset))


    // console.log("Listing " + tokenId + " with address: " + _tokenAddress + " from wallet: " + _bradsWallet)
    // return await seaport.createSellOrder({
    //     asset: {
    //         tokenId,
    //         tokenAddress: _tokenAddress,
    //     },
    //     accountAddress: _bradsWallet,
    //     startAmount: 0.0069,
    // })
}

// listForSale(1).then(listing => {
//     console.log(listing.asset.openseaLink);
// })
# AMC Ape Army Scripts

### Structure
- Assets
  - This contains the images and data required for generating the ape artwork
- Contracts
  - Smart contracts
- js-scripting
  - All of our javascript that is being used for listing NFTs on OpenSea
- Image_Generation.ipynb
  - This is the Jupyter Notebook file that was used to generate the NFT artwork and metadata

### Guides
I've created a minting guide [here](https://docs.google.com/document/d/1B-CGaOSE_dr35pkAISIWZZtEniGbYc6tyeexVvbcLN8/edit#).
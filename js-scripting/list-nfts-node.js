require('dotenv').config()
const opensea = require("opensea-js");
const OpenSeaPort = opensea.OpenSeaPort;
const Network = opensea.Network;
const MnemonicWalletSubprovider = require("@0x/subproviders").MnemonicWalletSubprovider;
const RPCSubprovider = require("web3-provider-engine/subproviders/rpc");
const Web3ProviderEngine = require("web3-provider-engine");

const MNEMONIC = process.env.MNEMONIC;
const NODE_API_KEY = process.env.INFURA_KEY || process.env.ALCHEMY_KEY;
const isInfura = !!process.env.INFURA_KEY;
const NFT_CONTRACT_ADDRESS = process.env.NFT_CONTRACT_ADDRESS;
const OWNER_ADDRESS = process.env.OWNER_ADDRESS;
const NETWORK = process.env.NETWORK;
const API_KEY = process.env.API_KEY || ""; // API key is optional but useful if you're doing a high volume of requests.

const BASE_DERIVATION_PATH = `44'/60'/0'/0`;

const mnemonicWalletSubprovider = new MnemonicWalletSubprovider({
    mnemonic: MNEMONIC,
    baseDerivationPath: BASE_DERIVATION_PATH,
});

console.log("> Connect to network")
const infuraRpcSubprovider = new RPCSubprovider({
    rpcUrl: isInfura
        ? "https://" + NETWORK + ".infura.io/v3/" + NODE_API_KEY
        : "https://" + NETWORK + ".alchemyapi.io/v2/" + NODE_API_KEY,
});

const providerEngine = new Web3ProviderEngine();
providerEngine.addProvider(mnemonicWalletSubprovider);
providerEngine.addProvider(infuraRpcSubprovider);
providerEngine.start();

console.log("> OpenSeaPort")

const seaport = new OpenSeaPort(
    providerEngine,
    {
        networkName: Network.Main,
        apiKey: API_KEY,
    },
    (arg) => console.log(arg)
);

if (!MNEMONIC || !NODE_API_KEY || !NETWORK || !OWNER_ADDRESS) {
    console.error(
        "Please set a mnemonic, Alchemy/Infura key, owner, network, API key, nft contract, and factory contract address."
    );
    return;
}

async function listForSale(tokenId) {
    return await seaport.createSellOrder({
        asset: {
            tokenId,
            NFT_CONTRACT_ADDRESS,
        },
        expirationTime: 0,
        accountAddress: OWNER_ADDRESS,
        startAmount: 0.0069,
    })
}

async function main() {
    console.log("> Listing");
    listForSale(1).then(listing => {
        console.log(listing.asset.openseaLink);
    });
}

// main();
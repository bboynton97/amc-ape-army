import * as Web3 from 'web3'
import { OpenSeaPort, Network } from 'opensea-js'

// This example provider won't let you make transactions, only read-only calls:
const provider = new Web3.providers.HttpProvider('window.ethereum')

const seaport = new OpenSeaPort(provider, {
    networkName: Network.Main
})

const _bradsWallet = 0xf7b2453D2846955Fc2123701C57c08351719bFf6
const _tokenAddress = 0x48Ea9f1efb967c326B29b2BB2cF9E0755Db355C8 // contract address

export async function listForSale(tokenId) {
    console.log("Listing ", tokenId)
    return await seaport.createSellOrder({
        asset: {
            tokenId,
            _tokenAddress,
        },
        _bradsWallet,
        startAmount: 0.0069,
    })
}

// listForSale(1).then(listing => {
//     console.log(listing.asset.openseaLink);
// })